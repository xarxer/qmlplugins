#include "jenkins.h"
#include "build.h"

#include <memory>

#include <QtCore/QTimer>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>
#include <QtCore/QDebug>

namespace Jenkins {

static std::deque<qint32> extractBuildNumbers(QJsonObject const& object) {
    std::deque<qint32> numbers;

    QJsonArray builds = object.value("builds").toArray();
    for(QJsonValueRef build_ref : builds) {
        QJsonObject build_object = build_ref.toObject();
        numbers.push_front(build_object.value("number").toInt());
    }

    return numbers;
}

template <typename InputFirst, typename InputSecond>
static std::pair<bool, InputFirst> last_element_ne(InputFirst first, InputSecond second) {
    std::pair<bool, InputFirst> result { false, first };

    while(*first < *second) {
        result.first = true;
        first++;
    }

    result.second = first;
    return result;
}

const QString Jenkins::BuildNumbersUrl = "job/%1/api/json?tree=builds[number]{0,%2}";
const QString Jenkins::BuildDataUrl    = "job/%1/%2/api/json?tree=displayName,duration,estimatedDuration,number,building,timestamp,result,actions[parameters[name,value]]";

Jenkins::Jenkins(QObject* parent) :
    QAbstractListModel(parent)
{
    QObject::connect(&network_manager_, &QNetworkAccessManager::finished, this, &Jenkins::onReplyReceived);

    poll_timer_id_ = this->startTimer(poll_interval_);

    QTimer::singleShot(0, this, &Jenkins::pollBuildNumbers);
}

QUrl const& Jenkins::url() const
{
    return jenkins_url_;
}

void Jenkins::setUrl(QUrl url)
{
    if(url == jenkins_url_) {
        return;
    }

    jenkins_url_ = std::move(url);
    emit urlChanged();
}

QString const& Jenkins::job() const
{
    return job_name_;
}

void Jenkins::setJob(QString const& job)
{
    if(job_name_ == job) {
        return;
    }

    job_name_ = job;
    emit jobChanged();
}

qint32 Jenkins::pollInterval() const
{
    return poll_interval_;
}

void Jenkins::setPollInterval(qint32 interval)
{
    if(poll_interval_ == interval) {
        return;
    }

    poll_interval_ = interval;

    if(poll_timer_id_ != 0) {
        this->killTimer(poll_timer_id_);
    }

    poll_timer_id_ = this->startTimer(poll_interval_);

    emit pollIntervalChanged();
}

quint32 Jenkins::buildCount() const
{
    return build_count_;
}

void Jenkins::setBuildCount(quint32 count)
{
    if(build_count_ == count) {
        return;
    }

    build_count_ = count;
    emit buildCountChanged();
}

int Jenkins::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return static_cast<int>(builds_.size());
}

QVariant Jenkins::data(const QModelIndex& index, int role) const
{
    if(index.isValid() == false || index.row() < 0 || static_cast<std::size_t>(index.row()) > builds_.size() - 1) {
        return QVariant();
    }

    auto const& build = std::next(builds_.begin(), index.row())->second;

    switch(role) {
        case Role::DisplayName: return build->displayName();
        case Role::Duration: return build->duration();
        case Role::EstimatedDuration: return build->estimatedDuration();
        case Role::Number: return build->number();
        case Role::Result: return build->result();
        case Role::Timestamp: return build->timestamp();
        case Role::Building: return build->building();
        case Role::ChangeId: return build->changeId();
        default: return QVariant();
    }
}

QHash<int, QByteArray> Jenkins::roleNames() const
{
    static QHash<int, QByteArray> role_names {
        {Role::DisplayName, "displayName" },
        {Role::Duration, "duration" },
        {Role::EstimatedDuration, "estimatedDuration" },
        {Role::Number, "number" },
        {Role::Result, "result" },
        {Role::Timestamp, "timestamp" },
        {Role::Building, "building" },
        {Role::ChangeId, "changeId" }
    };

    return role_names;
}

void Jenkins::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == poll_timer_id_) {
        pollBuildNumbers();
    }
}

void Jenkins::pollBuildNumbers()
{
    qDebug() << "Polling build numbers";
    QUrl request_url(QString("%1/%2").arg(jenkins_url_.toString(), BuildNumbersUrl.arg(job_name_).arg(build_count_)));
    network_manager_.get(QNetworkRequest(request_url));
}

void Jenkins::pollBuildData(qint32 number)
{
    qDebug() << "Polling build data for #" << number;
    QUrl request_url(QString("%1/%2").arg(jenkins_url_.toString(), BuildDataUrl.arg(job_name_).arg(number)));
    network_manager_.get(QNetworkRequest(request_url));
}

void Jenkins::onReplyReceived(QNetworkReply* reply)
{
    QByteArray reply_data = reply->readAll();
    reply->deleteLater();
    reply = nullptr;

    QJsonObject json_data = QJsonDocument::fromJson(reply_data).object();

    if(json_data.contains("builds")) {
        // This is a 'build numbers' update
        qDebug() << "Received build numbers update";
        handleBuildNumbersUpdate(json_data);
    } else {
        // This is a 'build data' update
        qDebug() << "Received build data update";
        handleBuildDataUpdate(json_data);
    }
}

void Jenkins::handleBuildNumbersUpdate(QJsonObject const& data)
{
    std::deque<qint32> jenkins_numbers = extractBuildNumbers(data);

    {   // Remove old builds
        for(auto it = builds_.begin(); it != builds_.end(); it++) {
            if(std::any_of(jenkins_numbers.begin(), jenkins_numbers.end(), [&](qint32 j_number) { return j_number == it->first; })) {
                continue;
            }

            int index = std::distance(builds_.begin(), it);

            qDebug() << "Removing build #" << it->second->number();

            this->beginRemoveRows(QModelIndex(), index, index);
            it = builds_.erase(it);
            this->endRemoveRows();
        }
    }

    {   // Add new builds
        for(qint32 number : jenkins_numbers) {
            if(builds_.find(number) == builds_.end()) {
                qDebug() << "Inserting build #" << number;
                this->beginInsertRows(QModelIndex(), builds_.size(), builds_.size());
                builds_.insert(std::make_pair(number, std::make_unique<Build>(number)));
                this->endInsertRows();

                pollBuildData(number);
            }
        }
    }
}

void Jenkins::handleBuildDataUpdate(QJsonObject const& data)
{
    qint32 number = data.value("number").toInt();
    auto subject = builds_.find(number);

    if(subject == builds_.end()) {
        qWarning() << "Tried to update build data for unknown build: " << number;
        return;
    }

    subject->second->updateData(data);

    QModelIndex changed_index = this->createIndex(std::distance(builds_.begin(), subject), 0);
    this->dataChanged(changed_index, changed_index);
}

}
