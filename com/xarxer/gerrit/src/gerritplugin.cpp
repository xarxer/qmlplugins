#include "gerritplugin.h"
#include "gerrit.h"
#include "change.h"

#include <QtQml/qqml.h>

void GerritPlugin::registerTypes(const char* uri)
{
    qmlRegisterType<Gerrit::Gerrit>(uri, 0, 1, "Gerrit");
    qmlRegisterType<Gerrit::Change>(uri, 0, 1, "GerritChange");
}
