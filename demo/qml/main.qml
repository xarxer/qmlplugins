import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import com.xarxer.smhi 0.1

ApplicationWindow {
    visible: true
    width: 640
    height: 480

    SMHI {
        id: smhi
        pollInterval: 20000
        latitude: 57.708
        longitude: 11.974

        Component.onCompleted: {
            update();
        }
    }

    ListView {
        anchors.fill: parent
        model: smhi
        delegate: RowLayout {
            Text { text: validTime }
            Text { text: airPressure }
        }
    }
}

