#pragma once

#include <QtCore/QUrl>
#include <QtCore/QObject>
#include <QtCore/QTimerEvent>
#include <QtCore/QAbstractListModel>

#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>

#include <set>

#include "weatherdata.h"

namespace SMHI {

class SMHI : public QAbstractListModel {

    Q_OBJECT

    Q_PROPERTY(qint32 pollInterval READ pollInterval WRITE setPollInterval NOTIFY pollIntervalChanged)
    Q_PROPERTY(double longitude READ longitude WRITE setLongitude NOTIFY longitudeChanged)
    Q_PROPERTY(double latitude READ latitude WRITE setLatitude NOTIFY latitudeChanged)

public:

    enum Role {
        ValidTime = Qt::UserRole + 1,
        AirPressure,
        AirTemperature,
        Visibility,
        WindDirection,
        WindSpeed,
        Humidity,
        ThunderProbability,
        WindGust,
        WeatherSymbol
    };

    SMHI(QObject* parent = nullptr);

    qint32 pollInterval() const;
    void setPollInterval(qint32 value);

    double longitude() const;
    void setLongitude(double value);

    double latitude() const;
    void setLatitude(double value);

    /* QAbstractItemModel interface */
    int rowCount(const QModelIndex& parent) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

public slots:

    void update();

signals:

    void pollIntervalChanged();
    void longitudeChanged();
    void latitudeChanged();
    void coordinatesChanged();

private slots:

    void pollData();
    void onPollIntervalChanged();
    void onReplyReceived(QNetworkReply* reply);

private:

    void timerEvent(QTimerEvent* event) override;
    void updateData(std::vector<WeatherData> const& new_data);

    QUrl createUrl();

    qint32 poll_interval_ = 0;
    qint32 poll_timer_id_ = 0;

    double longitude_ = 0.0;
    double latitude_ = 0.0;

    QNetworkAccessManager network_manager_;
    std::vector<WeatherData> weather_data_;
};

}
