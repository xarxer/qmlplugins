#include <QtQml/QQmlExtensionPlugin>

class JenkinsPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "com.xarxer.jenkins/0.1")

public:

    void registerTypes(const char* uri) override;

};
