#include "smhi.h"

#include <QtCore/QDebug>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>

namespace SMHI {

// https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/16.158/lat/58.5812/data.json

SMHI::SMHI(QObject* parent) :
    QAbstractListModel(parent)
{
    QObject::connect(this, &SMHI::pollIntervalChanged, this, &SMHI::onPollIntervalChanged);
    QObject::connect(&network_manager_, &QNetworkAccessManager::finished, this, &SMHI::onReplyReceived);
}

qint32 SMHI::pollInterval() const
{
    return poll_interval_;
}

void SMHI::setPollInterval(qint32 value)
{
    if(value == poll_interval_) {
        return;
    }

    poll_interval_ = value;
    emit pollIntervalChanged();
}

double SMHI::longitude() const
{
    return longitude_;
}

void SMHI::setLongitude(double value)
{
    if(qFuzzyCompare(value, longitude_)) {
        return;
    }

    longitude_ = value;

    emit longitudeChanged();
    emit coordinatesChanged();
}

double SMHI::latitude() const
{
    return latitude_;
}

void SMHI::setLatitude(double value)
{
    if(qFuzzyCompare(value, latitude_)) {
        return;
    }

    latitude_ = value;

    emit latitudeChanged();
    emit coordinatesChanged();
}

int SMHI::rowCount(const QModelIndex& parent) const
{
    return parent.isValid() ? 0 : weather_data_.size();
}

QVariant SMHI::data(const QModelIndex& index, int role) const
{
    if(index.row() < 0 || index.row() >= weather_data_.size() || index.isValid() == false) {
        return QVariant();
    }

    WeatherData const& subject = weather_data_[index.row()];
    switch(role) {
        case Role::ValidTime: return subject.valid_time;
        case Role::AirPressure: return subject.air_pressure;
        case Role::AirTemperature: return subject.air_temperature;
        case Role::Visibility: return subject.visibility;
        case Role::WindDirection: return subject.wind_direction;
        case Role::WindSpeed: return subject.wind_speed;
        case Role::Humidity: return subject.humidity;
        case Role::ThunderProbability: return subject.thunder_probability;
        case Role::WindGust: return subject.wind_gust;
        case Role::WeatherSymbol: return subject.weather_symbol;
        default: return QVariant();
    }
}

QHash<int, QByteArray> SMHI::roleNames() const
{
    static const QHash<int, QByteArray> roles {
        { ValidTime,          "validTime"           },
        { AirPressure,        "airPressure"         },
        { AirTemperature,     "airTemperature"      },
        { Visibility,         "visibility"          },
        { WindDirection,      "windDirection"       },
        { WindSpeed,          "windSpeed"           },
        { Humidity,           "humidity"            },
        { ThunderProbability, "thunderProbability"  },
        { WindGust,           "windGust"            },
        { WeatherSymbol,      "weatherSymbol"       }
    };

    return roles;
}

void SMHI::update()
{
    pollData();
}

void SMHI::pollData()
{
    network_manager_.get(QNetworkRequest(createUrl()));
}

void SMHI::onPollIntervalChanged()
{
    if(poll_timer_id_ != 0) {
        qDebug() << "Killing poll timer with id" << poll_timer_id_;

        this->killTimer(poll_timer_id_);
        poll_timer_id_ = 0;
    }

    if(poll_interval_ == 0) {
        qDebug() << "Poll interval set to 0. Polling is stopped";
        return;
    }

    poll_timer_id_ = this->startTimer(std::chrono::milliseconds(poll_interval_));

    qDebug() << "Started poll timer with id" << poll_timer_id_
             << "and a timeout of" << poll_interval_ << "milliseconds";
}

void SMHI::onReplyReceived(QNetworkReply* reply)
{
    QByteArray reply_data = reply->readAll();
    // int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    reply->deleteLater();

    QJsonObject json_data = QJsonDocument::fromJson(reply_data).object();
    qDebug() << "Received response with" << reply_data.size() << "bytes";

    std::vector<WeatherData> received_data;

    QJsonArray time_series = json_data.value("timeSeries").toArray();
    for(QJsonValueRef weather_ref : time_series) {
        QJsonObject weather_object = weather_ref.toObject();
        received_data.emplace_back(WeatherData::fromJson(weather_object));
    }

    updateData(received_data);
}

void SMHI::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == poll_timer_id_) {
        pollData();
    }
}

void SMHI::updateData(const std::vector<WeatherData>& new_data)
{
    // Remove old stuff first
    // A:       |4|5|6|7|8|9|10|
    // B: |1|2|3|4|5|6|
    // <some operation>
    //          ----------- Original objects
    //          |     |
    // B:       |4|5|6|7|8|9|10|

    // Remove old stuff
    {
        auto pos = std::find_if(weather_data_.begin(), weather_data_.end(),
                                [cmp = new_data.front().valid_time](auto& element) {
            return element.valid_time == cmp;
        });

        if(pos != weather_data_.begin()) {
            int first = 0;
            int last = std::distance(weather_data_.begin(), pos);
            qDebug() << "Removing from" << first << "to" << last;

            beginRemoveRows(QModelIndex(), first, last);
            weather_data_.erase(weather_data_.begin(), pos);
            endRemoveRows();
        }
    }

    // Update overlap
    {
        for(std::size_t i = 0; i < weather_data_.size(); i++) {
            if(weather_data_[i].update(new_data[i])) {
                qDebug() << "Element at" << i << "updated";
                emit dataChanged(index(static_cast<int>(i)), index(static_cast<int>(i)));
            }
        }
    }

    // Insert new stuff
    {
        qDebug() << "Inserting" << new_data.size() - weather_data_.size() << "elements";
        beginInsertRows(QModelIndex(), weather_data_.size(), new_data.size() - 1);
        std::copy(new_data.begin() + weather_data_.size(), new_data.end(), std::back_inserter(weather_data_));
        endInsertRows();
    }

    qDebug() << "Element count:" << weather_data_.size();
}

QUrl SMHI::createUrl()
{
    static const QString ForecastUrl = "https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/%1/lat/%2/data.json";
    QString result_string = ForecastUrl.arg(longitude_, 0, 'f', 3).arg(latitude_, 0, 'f', 3);
    return QUrl(result_string);
}

}
