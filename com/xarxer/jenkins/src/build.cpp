#include "build.h"

#include <QtCore/QVariant>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonValue>

namespace Jenkins {

Build::Build(qint32 number, QObject* parent) :
    QObject(parent),
    number_{number}
{ }

const QString& Build::displayName() const
{
    return display_name_;
}

qint64 Build::duration() const
{
    return duration_;
}

qint64 Build::estimatedDuration() const
{
    return estimated_duration_;
}

qint32 Build::number() const
{
    return number_;
}

Build::Result Build::result() const
{
    return result_;
}

const QDateTime& Build::timestamp() const
{
    return timestamp_;
}

bool Build::building() const
{
    return building_;
}

quint32 Build::changeId() const
{
    return change_id_;
}

void Build::updateData(QJsonObject const& data)
{
    QJsonArray actions = data.value("actions").toArray();

    for(QJsonValueRef action_ref : actions) {
        QJsonObject action_object = action_ref.toObject();

        if(action_object.value("_class").toString() == "hudson.plugins.gearman.NodeParametersAction") {
            QJsonArray parameters = action_object.value("parameters").toArray();
            for(QJsonValueRef parameter_ref : parameters) {
                QJsonObject parameter_object = parameter_ref.toObject();
                if(parameter_object.value("name").toString() == "ZUUL_CHANGE") {
                    change_id_ = parameter_object.value("value").toString().toInt();
                }
            }
        }
    }

    this->building_ = data.value("building").toBool();
    this->display_name_ = data.value("displayName").toString();
    this->duration_ = data.value("duration").toInt();
    this->estimated_duration_ = data.value("estimatedDuration").toInt();
    this->number_ = data.value("number").toInt();
    this->timestamp_ = QDateTime::fromMSecsSinceEpoch(data.value("timestamp").toVariant().toLongLong());

    QString result_str = data.value("result").toString();

    if(result_str == "SUCCESS") {
        this->result_ = Result::Success;
    } else if(result_str == "UNSTABLE") {
        this->result_ = Result::Unstable;
    } else if(result_str == "FAILURE") {
        this->result_ = Result::Failure;
    } else if(result_str == "NOT_BUILT") {
        this->result_ = Result::NotBuilt;
    } else if(result_str == "ABORTED") {
        this->result_ = Result::Aborted;
    }
}

void Build::parseData(QJsonObject const& data)
{
    QJsonArray actions = data.value("actions").toArray();

    QString triggered_by_change;

    for(QJsonValueRef action_ref : actions) {
        QJsonObject action_object = action_ref.toObject();

        if(action_object.value("_class").toString() == "hudson.plugins.gearman.NodeParametersAction") {
            QJsonArray parameters = action_object.value("parameters").toArray();
            for(QJsonValueRef parameter_ref : parameters) {
                QJsonObject parameter_object = parameter_ref.toObject();
                if(parameter_object.value("name").toString() == "ZUUL_CHANGE") {
                    triggered_by_change = parameter_object.value("value").toString();
                }
            }
        }
    }

    display_name_ = data.value("displayName").toString();
    duration_ = static_cast<uint32_t>(data.value("duration").toInt());
    estimated_duration_ = static_cast<uint32_t>(data.value("estimatedDuration").toInt());
    number_ = data.value("number").toInt();
    building_ = data.value("building").toBool();

    qint64 timestamp_value = data.value("timestamp").toVariant().toLongLong();
    timestamp_ = QDateTime::fromMSecsSinceEpoch(timestamp_value);

    QString result_str = data.value("result").toString();

    if(result_str == "SUCCESS") {
        result_ = Result::Success;
    } else if(result_str == "FAILURE") {
        result_ = Result::Failure;
    } else if(result_str == "UNSTABLE") {
        result_ = Result::Unstable;
    } else if(result_str == "NOT_BUILT") {
        result_ = Result::NotBuilt;
    } else if(result_str == "ABORTED") {
        result_ = Result::Aborted;
    } else {
        result_ = Result::Unknown;
    }
}

}
