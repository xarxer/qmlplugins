#pragma once

#include <QtCore/QUrl>
#include <QtCore/QObject>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QAuthenticator>
#include <QtNetwork/QNetworkReply>

#include <QtQml/QQmlParserStatus>

namespace Gerrit {

class Gerrit : public QObject, public QQmlParserStatus {

    Q_OBJECT

    Q_INTERFACES(QQmlParserStatus)

    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString username READ username WRITE setUsername NOTIFY usernameChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)

public:

    Gerrit(QObject* parent = nullptr);

    QUrl const& url() const;
    void setUrl(QUrl const& url);

    QString const& username() const;
    void setUsername(QString const& username);

    QString const& password() const;
    void setPassword(QString const& password);

    QNetworkReply* fetch(QString const& endpoint);

    /*** QQmlParserStatus interface ***/
    void classBegin() override;
    void componentComplete() override;

private slots:

    void onAuthRequired(QNetworkReply* reply, QAuthenticator* authenticator);

signals:

    void urlChanged();
    void usernameChanged();
    void passwordChanged();

private:

    QUrl gerrit_url_;

    QString username_;
    QString password_;

    QNetworkAccessManager network_manager_;
};

}
