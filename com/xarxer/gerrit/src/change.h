#pragma once

#include <QtCore/QObject>
#include <QtQml/QQmlParserStatus>
#include <QtNetwork/QNetworkReply>

#include "gerrit.h"

namespace Gerrit {

class Change : public QObject, public QQmlParserStatus
{

    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)

    Q_PROPERTY(Gerrit* source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(quint32 changeId READ changeId WRITE setChangeId NOTIFY dataChanged)
    Q_PROPERTY(QString name READ name NOTIFY dataChanged)
    Q_PROPERTY(QString subject READ subject NOTIFY dataChanged)

public:

    Gerrit* source() const;
    void setSource(Gerrit* instance);

    quint32 changeId() const;
    void setChangeId(quint32 id);

    QString const& name() const;
    QString const& subject() const;

    void classBegin() override;
    void componentComplete() override;

public slots:

    void update();

signals:

    void sourceChanged();
    void dataChanged();

private slots:

    void onReplyReceived();

private:

    Gerrit* gerrit_instance_ = nullptr;

    quint32 change_id_ = std::numeric_limits<quint32>::max();
    QString name_;
    QString subject_;

    QNetworkReply* network_reply_ = nullptr;
};

}
