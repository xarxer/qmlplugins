#include <QtQml/QQmlExtensionPlugin>

class SmhiPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "com.xarxer.smhi/0.1")

public:

    void registerTypes(const char* uri) override;

};
