find_package(Qt5 COMPONENTS Core Qml Quick Network REQUIRED)

add_qmlplugin(smhi
    URI com.xarxer.smhi
    VERSION 0.1
    SOURCES
        qmldir
        src/smhiplugin.h
        src/smhiplugin.cpp
        src/smhi.h
        src/smhi.cpp
        src/weatherdata.h
)

target_link_libraries(smhi Qt5::Core Qt5::Qml Qt5::Quick Qt5::Network)

set_target_properties(smhi PROPERTIES
    LINKER_LANGUAGE CXX
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED TRUE
    CXX_EXTENSIONS OFF
)
