#include "gerrit.h"

namespace Gerrit {

Gerrit::Gerrit(QObject* parent) :
    QObject(parent)
{
    QObject::connect(&network_manager_, &QNetworkAccessManager::authenticationRequired, this, &Gerrit::onAuthRequired);
}

QUrl const& Gerrit::url() const
{
    return gerrit_url_;
}

void Gerrit::setUrl(QUrl const& url)
{
    if(url == gerrit_url_) {
        return;
    }

    gerrit_url_ = url;
    emit urlChanged();
}

QString const& Gerrit::username() const
{
    return username_;
}

void Gerrit::setUsername(QString const& username)
{
    if(username == username_) {
        return;
    }

    username_ = username;
    emit usernameChanged();
}

QString const& Gerrit::password() const
{
    return password_;
}

void Gerrit::setPassword(QString const& password)
{
    if(password == password_) {
        return;
    }

    password_ = password;
    emit passwordChanged();
}

QNetworkReply* Gerrit::fetch(const QString& endpoint)
{
    QUrl request_url(QString("%1/%2").arg(gerrit_url_.toString(), endpoint));
    request_url.setUserName(username_);
    request_url.setPassword(password_);
    return network_manager_.get(QNetworkRequest(request_url));
}

void Gerrit::classBegin() { }

void Gerrit::componentComplete() { }

void Gerrit::onAuthRequired(QNetworkReply* reply, QAuthenticator* authenticator)
{
    authenticator->setUser(username_);
    authenticator->setPassword(password_);
}

}
