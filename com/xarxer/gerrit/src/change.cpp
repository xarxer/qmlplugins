#include "change.h"

#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>

namespace Gerrit {

static QByteArray MAGIC_PREFIX = ")]}'";

Gerrit* Change::source() const
{
    return gerrit_instance_;
}

void Change::setSource(Gerrit* instance)
{
    if(gerrit_instance_ == instance) {
        return;
    }

    gerrit_instance_ = instance;
    emit sourceChanged();
}

quint32 Change::changeId() const
{
    return change_id_;
}

void Change::setChangeId(quint32 id)
{
    if(change_id_ == id) {
        return;
    }

    change_id_ = id;
    update();
}

QString const& Change::name() const
{
    return name_;
}

QString const& Change::subject() const
{
    return subject_;
}

void Change::classBegin()
{ }

void Change::componentComplete()
{ }

void Change::update()
{
    if(network_reply_ != nullptr) {
        network_reply_->deleteLater();
        network_reply_ = nullptr;
    }

    if(gerrit_instance_ == nullptr) {
        return;
    }

    QString endpoint = QString("a/changes/%1?o=DETAILED_ACCOUNTS").arg(change_id_);
    network_reply_ = gerrit_instance_->fetch(endpoint);

    QObject::connect(network_reply_, &QNetworkReply::finished, this, &Change::onReplyReceived);
}

void Change::onReplyReceived()
{
    QByteArray reply_data = network_reply_->readAll();
    int http_status = network_reply_->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    network_reply_->deleteLater();
    network_reply_ = nullptr;

    if(reply_data.startsWith(MAGIC_PREFIX)) {
        reply_data = reply_data.mid(4);
    }

    QJsonObject json_data = QJsonDocument::fromJson(reply_data).object();
    name_ = json_data.value("owner").toObject().value("name").toString();
    subject_ = json_data.value("subject").toString();

    emit dataChanged();
}

}
