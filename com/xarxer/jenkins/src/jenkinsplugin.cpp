#include "jenkinsplugin.h"
#include "jenkins.h"
#include "build.h"

#include <QtQml/qqml.h>

void JenkinsPlugin::registerTypes(const char* uri)
{
    qmlRegisterType<Jenkins::Jenkins>(uri, 0, 1, "Jenkins");
    qmlRegisterUncreatableType<Jenkins::Build>(uri, 0, 1, "Build", "Builds are instantiated by Jenkins");
}
