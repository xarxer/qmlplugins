#pragma once

#include <QtCore/QUrl>
#include <QtCore/QObject>
#include <QtCore/QAbstractListModel>
#include <QtCore/QTimerEvent>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QAuthenticator>
#include <QtNetwork/QNetworkReply>

#include <QtQml/QQmlParserStatus>

#include <deque>
#include <set>
#include <memory>
#include <mutex>

#include "build.h"

namespace Jenkins {

class Jenkins : public QAbstractListModel {

    Q_OBJECT

    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString job READ job WRITE setJob NOTIFY jobChanged)
    Q_PROPERTY(qint32 pollInterval READ pollInterval WRITE setPollInterval NOTIFY pollIntervalChanged)
    Q_PROPERTY(quint32 buildCount READ buildCount WRITE setBuildCount NOTIFY buildCountChanged)

public:

    enum Role {
        DisplayName,
        Duration,
        EstimatedDuration,
        Number,
        Result,
        Timestamp,
        Building,
        ChangeId
    };

    Jenkins(QObject* parent = nullptr);

    QUrl const& url() const;
    void setUrl(QUrl url);

    QString const& job() const;
    void setJob(QString const& job);

    qint32 pollInterval() const;
    void setPollInterval(qint32 interval);

    quint32 buildCount() const;
    void setBuildCount(quint32 count);

    /* QAbstractListModel interface */
    int rowCount(const QModelIndex& parent) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

protected:

    void timerEvent(QTimerEvent* event) override;

private slots:

    void pollBuildNumbers();
    void pollBuildData(qint32 number);

    void onReplyReceived(QNetworkReply* reply);

signals:

    void urlChanged();
    void jobChanged();
    void pollIntervalChanged();
    void buildCountChanged();

private:

    struct BuildComparator {
        bool operator()(std::unique_ptr<Build> const& lhs, std::unique_ptr<Build> const& rhs) {
            return lhs->number() < rhs->number();
        }
    };

    void handleBuildNumbersUpdate(QJsonObject const& data);
    void handleBuildDataUpdate(QJsonObject const& data);

    QUrl jenkins_url_;
    QString job_name_;
    qint32 poll_interval_ = 60 * 1000;
    quint32 build_count_ = 5;

    qint32 poll_timer_id_ = 0;

    QNetworkAccessManager network_manager_;

    std::map<qint32, std::unique_ptr<Build>> builds_;

    static const QString BuildNumbersUrl;
    static const QString BuildDataUrl;
};

}
