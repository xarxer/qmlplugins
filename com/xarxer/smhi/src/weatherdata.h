#pragma once

#include <QtCore/QtGlobal>
#include <QtCore/QDateTime>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>

// ╔═══════════╦══════════╦════════════╦═══════════╦═════════════════════════════════════════╦═════════════════════════════╗
// ║ Parameter ║   Unit   ║ Level Type ║ Level (m) ║               Description               ║         Value range         ║
// ╠═══════════╬══════════╬════════════╬═══════════╬═════════════════════════════════════════╬═════════════════════════════╣
// ║ msl       ║ hPa      ║ hmsl       ║         0 ║ Air pressure                            ║ Decimal number, one decimal ║
// ║ t         ║ C        ║ hl         ║         2 ║ Air temperature                         ║ Decimal number, one decimal ║
// ║ vis       ║ km       ║ hl         ║         2 ║ Horizontal visibility                   ║ Decimal number, one decimal ║
// ║ wd        ║ degree   ║ hl         ║        10 ║ Wind direction                          ║ Integer                     ║
// ║ ws        ║ m/s      ║ hl         ║        10 ║ Wind speed                              ║ Decimal number, one decimal ║
// ║ r         ║ %        ║ hl         ║         2 ║ Relative humidity                       ║ Integer, 0-100              ║
// ║ tstm      ║ %        ║ hl         ║         0 ║ Thunder probability                     ║ Integer, 0-100              ║
// ║ tcc_mean  ║ octas    ║ hl         ║         0 ║ Mean value of total cloud cover         ║ Integer, 0-8                ║
// ║ lcc_mean  ║ octas    ║ hl         ║         0 ║ Mean value of low level cloud cover     ║ Integer, 0-8                ║
// ║ mcc_mean  ║ octas    ║ hl         ║         0 ║ Mean value of medium level cloud cover  ║ Integer, 0-8                ║
// ║ hcc_mean  ║ octas    ║ hl         ║         0 ║ Mean value of high level cloud cover    ║ Integer, 0-8                ║
// ║ gust      ║ m/s      ║ hl         ║        10 ║ Wind gust speed                         ║ Decimal number, one decimal ║
// ║ pmin      ║ mm/h     ║ hl         ║         0 ║ Minimum precipitation intensity         ║ Decimal number, one decimal ║
// ║ pmax      ║ mm/h     ║ hl         ║         0 ║ Maximum precipitation intensity         ║ Decimal number, one decimal ║
// ║ spp       ║ %        ║ hl         ║         0 ║ Percent of precipitation in frozen form ║ Integer, -9 or 0-100        ║
// ║ pcat      ║ category ║ hl         ║         0 ║ Precipitation category                  ║ Integer, 0-6                ║
// ║ pmean     ║ mm/h     ║ hl         ║         0 ║ Mean precipitation intensity            ║ Decimal number, one decimal ║
// ║ pmedian   ║ mm/h     ║ hl         ║         0 ║ Median precipitation intensity          ║ Decimal number, one decimal ║
// ║ Wsymb2    ║ code     ║ hl         ║         0 ║ Weather symbol                          ║ Integer, 1-27               ║
// ╚═══════════╩══════════╩════════════╩═══════════╩═════════════════════════════════════════╩═════════════════════════════╝

template<typename T, typename U = T>
bool exchange(T& obj, U&& new_value)
{
    if(obj == new_value) {
        return false;
    }

    obj = std::forward<U>(new_value);
    return true;
}

struct WeatherData {

    static WeatherData fromJson(QJsonObject const& data) {
        WeatherData result;

        result.valid_time = QDateTime::fromString(data.value("validTime").toString(), Qt::ISODate);
        QJsonArray paramArray = data.value("parameters").toArray();

        for(auto param : paramArray) {
            QJsonObject obj = param.toObject();
            QString paramName = obj.value("name").toString();
            QJsonValue paramValue = obj.value("values").toArray().first();

            if(paramName == "msl") {
                result.air_pressure = paramValue.toDouble();
            } else if(paramName == "t") {
                result.air_temperature = paramValue.toDouble();
            } else if(paramName == "vis") {
                result.visibility = paramValue.toDouble();
            } else if(paramName == "wd") {
                result.wind_direction = paramValue.toDouble();
            } else if(paramName == "ws") {
                result.wind_speed = paramValue.toDouble();
            } else if(paramName == "r") {
                result.humidity = paramValue.toDouble();
            } else if(paramName == "tstm") {
                result.thunder_probability = paramValue.toDouble();
            } else if(paramName == "gust") {
                result.wind_gust = paramValue.toDouble();
            } else if(paramName == "Wsymb2") {
                result.weather_symbol = paramValue.toDouble();
            } else if(paramName == "pmin") {
                result.precipitation.minimum_intensity = paramValue.toDouble();
            } else if(paramName == "pmax") {
                result.precipitation.maximum_intensity = paramValue.toDouble();
            } else if(paramName == "pmean") {
                result.precipitation.mean_intensity = paramValue.toDouble();
            } else if(paramName == "pmedian") {
                result.precipitation.median_intensity = paramValue.toDouble();
            } else if(paramName == "spp") {
                result.precipitation.frozen_form = paramValue.toDouble();
            } else if(paramName == "pcat") {
                result.precipitation.category = paramValue.toDouble();
            } else if(paramName == "tcc_mean") {
                result.clouds.total_cover = paramValue.toDouble();
            } else if(paramName == "lcc_mean") {
                result.clouds.low_cover = paramValue.toDouble();
            } else if(paramName == "mcc_mean") {
                result.clouds.medium_cover = paramValue.toDouble();
            } else if(paramName == "hcc_mean") {
                result.clouds.high_cover = paramValue.toDouble();
            }
        }

        return result;
    }

    bool update(WeatherData const& other) {
        bool changed = false;
        changed |= exchange(air_pressure, other.air_pressure);
        changed |= exchange(air_temperature, other.air_temperature);
        changed |= exchange(visibility, other.visibility);
        changed |= exchange(wind_direction, other.wind_direction);
        changed |= exchange(wind_speed, other.wind_speed);
        changed |= exchange(humidity, other.humidity);
        changed |= exchange(thunder_probability, other.thunder_probability);
        changed |= exchange(wind_gust, other.wind_gust);
        changed |= exchange(weather_symbol, other.weather_symbol);
        changed |= exchange(precipitation.minimum_intensity, other.precipitation.minimum_intensity);
        changed |= exchange(precipitation.maximum_intensity, other.precipitation.maximum_intensity);
        changed |= exchange(precipitation.mean_intensity, other.precipitation.mean_intensity);
        changed |= exchange(precipitation.median_intensity, other.precipitation.median_intensity);
        changed |= exchange(precipitation.frozen_form, other.precipitation.frozen_form);
        changed |= exchange(precipitation.category, other.precipitation.category);
        changed |= exchange(clouds.total_cover, other.clouds.total_cover);
        changed |= exchange(clouds.low_cover, other.clouds.low_cover);
        changed |= exchange(clouds.medium_cover, other.clouds.medium_cover);
        changed |= exchange(clouds.high_cover, other.clouds.high_cover);

        return changed;
    }

    QDateTime valid_time;

    double air_pressure = 0.0;
    double air_temperature = 0.0;
    double visibility = 0.0;
    qint16 wind_direction = 0;
    double wind_speed = 0.0;
    qint8 humidity = 0;
    qint8 thunder_probability = 0;
    double wind_gust = 0.0;
    qint8 weather_symbol = 0;

    struct Precipitation {
        double minimum_intensity = 0.0;
        double maximum_intensity = 0.0;
        double mean_intensity = 0.0;
        double median_intensity = 0.0;
        qint8 frozen_form = 0;
        qint8 category = 0;
    } precipitation;

    struct CloudCover {
        qint8 total_cover = 0;
        qint8 low_cover = 0;
        qint8 medium_cover = 0;
        qint8 high_cover = 0;
    } clouds;
};
