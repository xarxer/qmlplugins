#include "smhiplugin.h"
#include "smhi.h"

#include <QtQml/qqml.h>

void SmhiPlugin::registerTypes(const char* uri)
{
    qmlRegisterType<SMHI::SMHI>(uri, 0, 1, "SMHI");
}
