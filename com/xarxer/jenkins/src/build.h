#pragma once

#include <QtCore/QUrl>
#include <QtCore/QTimer>
#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtCore/QJsonObject>

#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>

namespace Jenkins {

class Build : public QObject
{

    Q_OBJECT

public:

    enum Result {
        Unknown,
        Success,
        Unstable,
        Failure,
        NotBuilt,
        Aborted
    }; Q_ENUMS(Result)

    Build(qint32 number, QObject* parent = nullptr);

    QString const& displayName() const;
    qint64 duration() const;
    qint64 estimatedDuration() const;
    qint32 number() const;
    Result result() const;
    QDateTime const& timestamp() const;
    bool building() const;
    quint32 changeId() const;

    void updateData(QJsonObject const& data);

private slots:

    void onDataReceived();

private:

    void parseData(QJsonObject const& data);

    QString display_name_;
    qint64 duration_ = 0;
    qint64 estimated_duration_ = 0;
    qint32 number_ = 0;
    Result result_ = Result::Unknown;
    QDateTime timestamp_;
    bool building_ = false;
    quint32 change_id_ = 0;
};

}
