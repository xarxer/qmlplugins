find_package(Qt5 COMPONENTS Core Qml Quick REQUIRED)

add_executable(qmlplugins-demo
    resources.qrc
    src/main.cpp
)

target_link_libraries(qmlplugins-demo Qt5::Core Qt5::Qml Qt5::Quick)

set_target_properties(qmlplugins-demo PROPERTIES
    LINKER_LANGUAGE CXX
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED TRUE
    CXX_EXTENSIONS OFF
    AUTORCC ON
)

set(QML_IMPORT_PATH ${CMAKE_BINARY_DIR} CACHE STRING "" FORCE)
