#include <QtQml/QQmlExtensionPlugin>

class GerritPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "com.xarxer.gerrit/0.1")

public:

    void registerTypes(const char* uri) override;

};
